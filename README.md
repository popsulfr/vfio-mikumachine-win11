# vfio mikumachine-win11

Single GPU passthrough

## Machine

* AMD Ryzen 9 5900X
* Gigabyte Aorus X570 Master
* MSI GeForce GTX 980 Ti GAMING 6G
* 32GB RAM
* 2TB Aorus NVMe SSD

## Links

- The ultimate vfio resource: https://wiki.archlinux.org/index.php/PCI_passthrough_via_OVMF
- Single GPU passthrough: https://github.com/joeknock90/Single-GPU-Passthrough
- SomeOrdinaryGamer's incredibly helpful video walkthrough: https://www.youtube.com/watch?v=BUSrdUoedTo

## Custom Setup

My custom working setup is shared in this repo but basically it's Mutahar's (SomeOrdinaryGamers) video tutorial, with some minor changes to the hooks.
It applies the Arch wiki's performance tips like cpu pinning, using virtio for disk and network adapter.

Windows 11 needs Secure Boot and a TPM, which is easy to enable using `swtpm`.

## Troubleshooting/Tricks

### Nvidia removed code 43 on their drivers but features are still missing (still the case?)

Following is still needed to unlock all the nvidia drivers features:

```xml
  <features>
    <hyperv>
      <vendor_id state='on' value='randomid'/>
    </hyperv>
    <kvm>
      <hidden state='on'/>
    </kvm>
  </features>
```

For instance I couldn't reach 144Hz on my monitor but was limited to 85Hz without these options.
Other entries and options like custom resolutions were also gone.

### Enable AMD SM

```xml
  <cpu>
    <feature policy='require' name='topoext'/>
  </cpu>
```

### Btrfs

I created a subvolume at `/var/lib/libvirt/images` to avoid automatic snapshots on the vm images:

```sh
sudo rmdir /var/lib/libvirt/images
sudo btrfs subvolume create /var/lib/libvirt/images
```

I also disabled COW on that folder:

```sh
sudo chattr +C /var/lib/libvirt/images
```

It might be better to use a raw image file with `falloc` on btrfs, but I still wanted the flexibility of qcow2 with dynamic size.
Using VirtIO Disk by providing the driver during install gives great performance.

### qcow2 with zstd compression

```sh
sudo qemu-img create -f qcow2 -o compression_type=zstd,preallocation=metadata,lazy_refcounts=on <image.qcow2> <size>
```

e.g.:
```sh
qemu-img create -f qcow2 -o compression_type=zstd,preallocation=metadata,lazy_refcounts=on /var/lib/libvirt/images/win11.qcow2 256g
```

* On non-COW filesystems like btrfs using `preallocation=metadata` will give a bit better performance without allocating the whole space.
* On btrfs using `preallocation=falloc` and disabling CoW will theoretically give the best performance but allocates the whole space. `metadata` still gives reasonable performance.

### Error when creating a snapshot

Set `type='pflash'` to `type='rom'` for the loader temporarily, take the snapshot and set it back.

```xml
<domain>
  ...
  <os>
    ...
    <loader readonly='yes' secure='yes' type='rom'>/usr/share/edk2-ovmf/x64/OVMF_CODE.secboot.fd</loader>
    ...
  </os>
  ...
</domain>
```

### Tune virtio disk driver

Use the recent io type `io_uring` for great performance gains + `writeback` for the cache.

```xml
<domain>
  ...
  <devices>
    ...
    <disk type='file' device='disk'>
      <driver name='qemu' type='qcow2' cache='writeback' io='io_uring' discard='unmap' iothread='1' queues='20'/>
      <source file='/mnt/libvirt-windows/libvirt/images/win11.qcow2'/>
      <target dev='vda' bus='virtio'/>
    </disk>
    ...
  </devices>
  ...
</domain>
```

### Passthrough the host's filesystem

Using `virtiofs` one can pass a folder on the host as drive in Windows.

- https://libvirt.org/kbase/virtiofs.html
- https://virtio-fs.gitlab.io/

```xml
<domain>
  ...
  <memoryBacking>
    <source type='memfd'/>
    <access mode='shared'/>
  </memoryBacking>
  ...
  <devices>
    ...
    <filesystem type='mount' accessmode='passthrough'>
      <driver type='virtiofs' queue='1024'/>
      <source dir='/home/popsulfr/Downloads'/>
      <target dir='Downloads'/>
    </filesystem>
    ...
  </devices>
  ...
</domain>
```

Make sure to set the queue to 1024 : `queue='1024'` to avoid errors when accessing big files.

From my testing, it's far from being as performant as a Samba share on the host but still reasonable.
The shared folder shows up as its own drive (`Z:\` by default), so that might be a benefit in some applications compared to a Samba share.

### Pulseaudio/Pipewire share with host

QEMU needs to be run as the user that owns the pulseaudio socket or the socket needs to be rw for everyone:

`/etc/libvirt/qemu.conf`:

```
user = "popsulfr"
```

```xml
<domain>
  ...
  <devices>
    <sound model='ich9'>
      <codec type='micro'/>
      <audio id='1'/>
    </sound>
    <audio id='1' type='pulseaudio' serverName='/run/user/1000/pulse/native'/>
  </devices>
  ...
</domain>
```

### ipv6 NAT for guests

- https://libvirt.org/formatnetwork.html#examplesNATv6

My default network config:

```xml
<network>
  <name>default</name>
  <uuid>adeb48d9-261d-4fe8-8ba5-77638b639c4f</uuid>
  <forward mode='nat'>
    <nat ipv6='yes'/>
  </forward>
  <bridge name='virbr0' stp='on' delay='0'/>
  <mac address='52:54:00:d5:0a:2a'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
    </dhcp>
  </ip>
  <ip family='ipv6' address='fd50:54ff:fed5:a2a::1' prefix='64'>
  </ip>
</network>
```

To create the `fd50:54ff:fed5:a2a::1` address I used the `ipv6calc` tool to infer eui64 from the mac address `52:54:00:d5:0a:2a` assigned to the bridge `virbr0` then just appended it to `fd`.

Now to get full ipv6 preference in Windows with these addresses following needs to be done:

- https://superuser.com/questions/1469774/why-does-windows-10-prefer-ipv4-over-ipv6/1469778#1469778
- https://www.reddit.com/r/ipv6/comments/coei3n/fix_for_windows_10_preferring_ipv4_over_ipv6/?utm_source=share&utm_medium=web2x&context=3

Basically in powershell with admin privileges do:

```
netsh interface ipv6 add prefixpolicy fd00::/8 3 1
```

You should be in the green now on ipv6 test sites like https://test-ipv6.com/ or https://ipv6-test.com/ or https://ipv6test.google.com/.

## Additional Windows 11 tips

### Install without connecting to the Internet

At one point during the installation, the setup insists you connect to the internet.
You can skip that entirely by pressing `Shift + F10` which will open the command prompt and typing:

```sh
taskkill /F /IM oobenetworkconnectionflow.exe
```

### Block Spying and Tracking using WindowsSpyBlocker

https://github.com/crazy-max/WindowsSpyBlocker

The update list can be used to stop the Windows Update harassment if needed.

Instead of using the tool, the lists can also be loaded into dnscrypt-proxy or pi.hole.
