#!/bin/bash
# Helpful to read output when debugging
set -x

# load variables we defined
source /etc/libvirt/hooks/kvm.conf

# Terminate sessions safely
for s in $(loginctl --no-legend list-sessions | cut -d' ' -f1)
do
	d="$(loginctl show-session "$s" --property=Desktop --value)"
	[[ -n "$d" ]] && loginctl terminate-session "$s"
done

# Stop display manager
systemctl stop sddm.service

# stop all pipewire user instances
for u in $(loginctl --no-legend list-users | cut -d' ' -f2)
do
	systemctl --user --machine="$u"@ stop pipewire.service
done

# kill all processes still using the nvidia driver
lsof -t /dev/nvidia* 2>/dev/null | xargs -r kill --timeout 1000 KILL 2>/dev/null

# Unbind VTconsoles
grep -l 'frame buffer' /sys/class/vtconsole/vtcon*/name | while read -r v
do
	echo 0 > "$(dirname "$v")"/bind
done

# Unbind EFI-Framebuffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/unbind

while true
do
	m=""
	for mr in "${MODULES[@]}"
	do
		for m in $(lsmod | sed -n 's/^\('"${mr}"'\)\s.*[0-9]\s*$/\1/p')
		do
			modprobe -r "$m" && echo "$m" >> /tmp/modules
		done
	done
	[[ -z "$m" ]] && break
done

# Detach the pci devices
for pci_dev in "${VIRSH_PCI_DEVS[@]}"
do
	virsh nodedev-detach "$pci_dev"
done

# load vfio
for m in vfio vfio_pci vfio_iommu_type1
do
	modprobe "$m"
done

# isolate cpus
systemctl set-property --runtime -- user.slice AllowedCPUs="$AllowedCPUs"
systemctl set-property --runtime -- system.slice AllowedCPUs="$AllowedCPUs"
systemctl set-property --runtime -- init.scope AllowedCPUs="$AllowedCPUs"

