#!/bin/bash
# Helpful to read output when debugging
set -x

# Load variables
source /etc/libvirt/hooks/kvm.conf

systemctl set-property --runtime -- user.slice AllowedCPUs="$ResetAllowedCPUs"
systemctl set-property --runtime -- system.slice AllowedCPUs="$ResetAllowedCPUs"
systemctl set-property --runtime -- init.scope AllowedCPUs="$ResetAllowedCPUs"

# unload vfio-pci
for m in vfio_pci vfio_iommu_type1 vfio
do
	modprobe -r "$m"
done

# Rebind PCI devs
for pci_dev in "${VIRSH_PCI_DEVS[@]}"
do
	virsh nodedev-reattach "$pci_dev"
done

# rebind VTconsoles
grep -l 'frame buffer' /sys/class/vtconsole/vtcon*/name | while read -r v
do
        echo 1 > "$(dirname "$v")"/bind
done

# Read nvidia x config
nvidia-xconfig --query-gpu-info > /dev/null 2>&1

# Bind EFI-framebuffer
echo efi-framebuffer.0 > /sys/bus/platform/drivers/efi-framebuffer/bind

# Reload modules
tac /tmp/modules | while read -r m
do
	modprobe "$m"
done
rm -f /tmp/modules

# Restart Display service
systemctl start sddm.service
